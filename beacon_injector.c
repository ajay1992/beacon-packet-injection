/*
created by Ajay
copyright all rights reserved

put the interface in monitor mode and inject the packet using this program

Syntax:./a.out [interface] [ssid] [encryption type]

[encryption type]=0:no encryption,1:wep,2:wpa
eg: ./a.out mon0 "fake_ap" 1
*/
#include<stdio.h>
#include<linux/types.h>
#include<stdlib.h>
#include<string.h>
#include<strings.h>
#include<netinet/ether.h>
#include<sys/socket.h>
#include<sys/ioctl.h>
#include<linux/if_packet.h>
#include<linux/if_ether.h>
#include<errno.h>
#include<net/ethernet.h>
#include<arpa/inet.h>
#include<netinet/in.h>
#include<net/if.h>

#define SSID "a2:c3:11:b2:23:d1"
#define BSSID "ff:ff:ff:ff:ff:ff"

//beacon frame
typedef struct bframe{
	unsigned short frame_control;
	unsigned short duration;
	unsigned char daddr[6];
	unsigned char saddr[6];
	unsigned char bssid[6];
	unsigned short seq;
}bframe;
//wlan management frame
typedef struct wlanm{
	long timestamp;
	unsigned short interval;
	unsigned short capability;
	unsigned short tag;
}wlanm;



unsigned char *CreateBeacon(char *ssid, char *bssid,unsigned char name[], int encryption, int *size){
	
	char *radiotap="\x00\x00\x12\x00\x2e\x48\x00\x00\x00\x02\x71\x09\xa0\x00\xdb\x06\x00\x00";//radiotap frame

	//variable information if no encryption
	char *wlan_noenc="\x01\x08\x82\x84\x8b\x96\x8c\x12\x98\x24\x03\x01\x02\x05\x04\x00\x01\x00"
	"\x00\x2a\x01\x00\x32\x04\xb0\x48\x60\x6c\xdd\x18\x00\x50\xf2\x02\x01\x01\x83\x00\x03\xa4"
	"\x00\x00\x27\xa4\x00\x00\x42\x43\x5e\x00\x62\x32\x2f\x00\xdd\x1e\x00\x90\x4c\x33\x4c\x10"
	"\x1b\xff\xff\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"
	"\x00\x00\x2d\x1a\x4c\x10\x1b\xff\xff\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"
	"\x00\x00\x00\x00\x00\x00\x00\x00\xdd\x1a\x00\x90\x4c\x34\x02\x08\x08\x00\x00\x00\x00\x00"
	"\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x3d\x16\x02\x08\x08\x00\x00\x00"
	"\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x4a\x0e\x14\x00\x0a\x00"
	"\x2c\x01\xc8\x00\x14\x00\x05\x00\x19\x00\x7f\x01\x01\xdd\x09\x00\x03\x7f\x01\x01\x00\x00"
	"\xff\x7f\xdd\x0a\x00\x03\x7f\x04\x01\x00\x00\x00\x40\x00";
	
	//variable information for wep encryption
	char *wlan_wep="\x01\x08\x82\x84\x8b\x96\x8c\x12\x98\x24\x03\x01\x02\x05\x04\x00\x01\x00"
	"\x00\x2a\x01\x00\x32\x04\xb0\x48\x60\x6c\xdd\x18\x00\x50\xf2\x02\x01\x01\x85\x00\x03\xa4"
	"\x00\x00\x27\xa4\x00\x00\x42\x43\x5e\x00\x62\x32\x2f\x00\xdd\x09\x00\x03\x7f\x01\x01\x00"
	"\x00\xff\x7f\xdd\x0a\x00\x03\x7f\x04\x01\x00\x00\x00\x40\x00";

	//variable information for wpa encryption
	char *wlan_wpa="\x01\x08\x82\x84\x8b\x96\x8c\x12\x98\x24\x03\x01\x02\x05\x04\x00\x01\x00"
	"\x00\x2a\x01\x00\xdd\x16\x00\x50\xf2\x01\x01\x00\x00\x50\xf2\x02\x01\x00\x00\x50\xf2\x02"
	"\x01\x00\x00\x50\xf2\x02\x32\x04\xb0\x48\x60\x6c\xdd\x18\x00\x50\xf2\x02\x01\x01\x84\x00"
	"\x03\xa4\x00\x00\x27\xa4\x00\x00\x42\x43\x5e\x00\x62\x32\x2f\x00\xdd\x09\x00\x03\x7f\x01"
	"\x01\x00\x00\xff\x7f\xdd\x0a\x00\x03\x7f\x04\x01\x00\x00\x00\x40\x00";


	bframe *bfr;
	wlanm *wlanfr;
	char *pack;
	int len;
	bfr=(bframe*)malloc(sizeof(bframe));
	bfr->frame_control=htons(0x8000);
	bfr->duration=htons(0);
	memcpy(bfr->daddr,(void *)ether_aton(bssid),6);
	memcpy(bfr->saddr,(void *)ether_aton(ssid),6);
	memcpy(bfr->bssid,(void *)ether_aton(ssid),6);
	bfr->seq=htons(0);
	
	wlanfr=(char *)malloc(14);
	wlanfr->timestamp=htonl(time(NULL));
	wlanfr->interval=htons(0x6400);
	wlanfr->tag=htons((short)strlen(name));
	


	switch(encryption){
		case 0:
		{
			
			len=14+208+strlen(name);
			pack=(char*)malloc(18+sizeof(bframe)+len);
			memcpy(pack+18+sizeof(bframe)+14+strlen(name),wlan_noenc,208);
			wlanfr->capability=htons(0x2104);
			*size=len+24+18;
			break;
		}
		case 1:
		{
			len=14+77+strlen(name);
			pack=(char*)malloc(18+sizeof(bframe)+len);
			memcpy(pack+18+sizeof(bframe)+14+strlen(name),wlan_wep,77);
			wlanfr->capability=htons(0x3104);
			*size=len+24+18;
			break;
		}
		case 2:
		{
			len=14+101+strlen(name);
			pack=(char*)malloc(18+sizeof(bframe)+len);
			memcpy(pack+18+sizeof(bframe)+14+strlen(name),wlan_wpa,101);
			wlanfr->capability=htons(0x3104);
			*size=len+24+18;
			break;
		}
		default:
		{
			len=14+208+strlen(name);
			pack=(char*)malloc(18+sizeof(bframe)+len);
		        memcpy(pack+18+sizeof(bframe)+14+strlen(name),wlan_noenc,208);
			wlanfr->capability=htons(0x2104);
			*size=len+24+18;
		}	
	}

        memcpy(pack,radiotap,18);
        memcpy(pack+18,(char*)bfr,sizeof(bframe));
        memcpy(pack+18+sizeof(bframe),(char *)wlanfr,sizeof(wlanm));
        memcpy(pack+18+sizeof(bframe)+14,name,strlen(name));
	
	return (unsigned char*)pack;
}

int CreateRawSocket(int protocol){
	int rawsocket;
	if((rawsocket=socket(PF_PACKET,SOCK_RAW,htons(protocol)))==-1){
		perror("rawsocket:");
		exit(-1);
	}
	return rawsocket;
}	

int BindRawSocket(int rawsocket,char *interface, int protocol){
	struct sockaddr_ll sll;
	struct ifreq ifr;

	bzero(&ifr, sizeof(struct ifreq));
	bzero(&sll, sizeof(struct sockaddr_ll));

	strncpy((char*)ifr.ifr_name, interface, IFNAMSIZ);
	if(ioctl(rawsocket, SIOCGIFINDEX, &ifr)==-1){
		perror("ioctl:");
		exit(-1);
	}
	sll.sll_family=AF_PACKET;
	sll.sll_protocol=htons(protocol);
	sll.sll_ifindex=ifr.ifr_ifindex;

	if(bind(rawsocket, (struct sockaddr*)&sll, sizeof(struct sockaddr_ll))==-1){
		perror("bind:");
		exit(-1);
	}
	return 1;
}

int SendPacket(unsigned char *buffer, int len, int rawsocket){
	int sent=0;
	if((sent=write(rawsocket, buffer, len)) != len){
		printf("Error writting packet\n");
		exit(-1);
	}
	printf("success\n");
	return 1;
	
}

void main(int argc, char **argv){
	unsigned char *pack;
	int len,i=20;
	int rawsocket;
	rawsocket=CreateRawSocket(ETH_P_ALL);
	BindRawSocket(rawsocket,argv[1],ETH_P_ALL);
	pack=CreateBeacon(SSID,BSSID,argv[2],atoi(argv[3]), &len);
	while(i--){
		SendPacket(pack,len,rawsocket);
	}
	free(pack);
}
